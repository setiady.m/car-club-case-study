const PORT = 3000,
			express = require('express'),
			app = express(),
			bodyParser = require('body-parser'),
			user = require('./routes/user.route'),
			mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/carclubcasestudy');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/uploads", express.static(__dirname + '/uploads'));
app.use('/user', user);
app.listen(PORT, function(){
   console.log('Server is running on Port',PORT);
});