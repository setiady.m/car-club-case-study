const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
let multer  = require('multer');  

var Storage = multer.diskStorage({
  destination: function(req, file, callback) {
    callback(null, "uploads/");
  },
  filename: function(req, file, callback) {
    callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
  }
});

let upload  = multer({ storage: Storage });

router.post('/signup', upload.single('photo'), (req, res) => {  
  bcrypt.hash(req.body.password, 10, function(err, hash){
     if(err) {
        return res.status(500).json({
           error: err
        });
     } else {
      // console.log(req.body, req.file)
      const user = new User({
         _id: new  mongoose.Types.ObjectId(),
         fullname: req.body.fullname,
         photo: req.file.path,
         email: req.body.email,
         password: hash
      });
      user.save().then(function(result) {
        res.status(200).json({
          success: 'New user has been created'
        });
      }).catch(error => {
         res.status(500).json({
            error: err
         });
      });
     }
  });
});

router.post('/signin', function(req, res){
   User.findOne({email: req.body.email})
   .exec()
   .then(function(user) {
      bcrypt.compare(req.body.password, user.password, function(err, result){
         if(err) {
            return res.status(401).json({
               failed: 'Unauthorized Access'
            });
         }
         if(result) {
            const JWTToken = jwt.sign({
               email: user.email,
               _id: user._id
            },
            'secret',
               {
                  expiresIn: '2h'
               });
            return res.status(200).json({
               success: 'Welcome',
               token: JWTToken,
               auth: user
            });
         }
         return res.status(401).json({
            failed: 'Unauthorized Access'
         });
      });
   })
   .catch(error => {
      res.status(500).json({
         error: error
      });
   });;
});


router.post('/logout', function(req, res) {
  console.log('Logout');
  return res.json({ status: true })
})

module.exports = router;