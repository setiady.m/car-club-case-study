import { CarClubCaseStudyPage } from './app.po';

describe('car-club-case-study App', () => {
  let page: CarClubCaseStudyPage;

  beforeEach(() => {
    page = new CarClubCaseStudyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
