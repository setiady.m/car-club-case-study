import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})

export class SignUpComponent implements OnInit {
  @ViewChild('fileInput') fileInput;

  public frm: FormGroup;
  public photo = {};
  public isBusy = false;
  public hasFailed = false;
  public showInputErrors = false;

  constructor(
    private api: ApiService,
    private auth: AuthService,
    private fb: FormBuilder,
    private router: Router
  ) {
    this.frm = fb.group({
    	fullname: ['', Validators.required],
    	photo: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {}

  uploadPhoto() {
    let fileBrowser = this.fileInput.nativeElement;
    if (fileBrowser.files && fileBrowser.files[0]) {
      const formData = new FormData();
      this.photo = fileBrowser.files[0]
    }
  }

  isEmpty(obj) {
    for(var key in obj) {
      if(obj.hasOwnProperty(key))
          return false;
    }
    return true;
  }

  public doSignUp(user){
    var formData = new FormData()
    formData.append('fullname', user.fullname)
    formData.append('email', user.email)
    formData.append('photo', this.photo)
    formData.append('password', user.password)    
    // Make sure form values are valid
    // if (this.frm.invalid) {
    //   this.showInputErrors = true;
    //   return;
    // }

    // // Reset status
    // this.isBusy = true;
    // this.hasFailed = false;

    // // Grab values from form
    // const fullname = this.frm.get('fullname').value,
    // 			email = this.frm.get('email').value,
    // 			password_confirmation = this.frm.get('password').value,
    // 			password = this.frm.get('password').value;

    // Submit request to API
    this.api.signUp(formData).subscribe(
      (response) => {
        console.log('true');
        this.router.navigate(['sign-in']);
      },
      (error) => {
        console.log('failed')
        this.isBusy = false;
        this.hasFailed = true;
      }
    );
  }
}