import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { SessionService } from './session.service';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
const API_URL = environment.apiUrl;

@Injectable()
export class AuthService {

  constructor(
    private session: SessionService,
    private router: Router
  ) {
  }

  public isSignedIn() {
    return !!this.session.accessToken;
  }

  public doSignOut() {
    this.session.destroy();
    this.router.navigate(['sign-in']);

  }

  public doSignIn(accessToken: string, name: string, auth: {}) {
    if ((!accessToken) || (!name)) return;
    this.session.accessToken = accessToken;
    this.session.auth = auth
  }
}