import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ApiService } from '../api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { SessionService } from '../../app/session.service';
import { environment } from 'environments/environment';
declare var $: any;

@Component({
  selector: 'app-rate-calculator',
  templateUrl: './rate-calculator.component.html',
  styleUrls: ['./rate-calculator.component.css']
})

export class RateCalculatorComponent implements OnInit {
  public frm: FormGroup;
  public isBusy = false;
  public hasFailed = false;
  public showInputErrors = false;
  public full_url = environment.apiUrl;

	constructor(
    private api: ApiService,
    private auth: AuthService,
    private fb: FormBuilder,
    private router: Router,
    private session: SessionService
  ) {
    this.frm = fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  ngOnInit() {}

  doLogout() {    
    this.auth.doSignOut();
  }

  ngAfterViewInit(){
    var script = document.createElement('script')
    script.onload = function() {
      console.log("Script loaded and ready")
    }
    script.src = '/assets/js/script.js'
    document.getElementsByTagName('body')[0].appendChild(script)

    setTimeout(() => {
      $('#car_category').multiselect({ nonSelectedText: 'Select Your Car' });
    }, 1500)     
  }
}