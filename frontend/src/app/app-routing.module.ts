import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { RateCalculatorComponent } from './rate-calculator/rate-calculator.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { CanActivateTodosGuard } from './can-activate-todos.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'sign-in',
    pathMatch: 'full'
  }, {
    path: 'sign-up',
    component: SignUpComponent
  }, {
    path: 'sign-in',
    component: SignInComponent
  }, {
    path: 'rate-calculator',
    component: RateCalculatorComponent,
    canActivate: [
      CanActivateTodosGuard
    ],
  },{
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    CanActivateTodosGuard
  ]
})
export class AppRoutingModule { }
