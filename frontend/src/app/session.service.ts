import { Injectable } from '@angular/core';

@Injectable()
export class SessionService {

  public accessToken: string;
  public name: string;
  public auth: {};

  constructor() {
  }

  public destroy(): void {
    this.accessToken = null;
    this.name = null;
    this.auth = {};
  }
}
