module.exports = function(grunt) {
	grunt.initConfig({
	  pkg: grunt.file.readJSON("package.json"),
	  less: {
		  development: {
		    options: {
		      paths: ['src/assets/css']
		    },
		    files: {
		      'src/assets/css/bootstrap-timepicker.css': 'bower_components/bootstrap-timepicker/css/timepicker.less'
		    }
		  }
		},
	  uglify: {
	    options: {
	      mangle: true
	    },
	    my_target: {
	      files: {
	        'src/assets/js/script.js': [
		      	'bower_components/jquery/dist/jquery.slim.js',
						'node_modules/popper.js/dist/umd/popper.min.js',
		      	'bower_components/bootstrap/dist/js/bootstrap.js',
		      	'bower_components/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
		      	'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
		      	'bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js',
		      	'src/assets/js/custom.js'
		      ]
	      }
	    }
	  },
	  cssmin: {
		  options: {
		    mergeIntoShorthands: false,
		    roundingPrecision: -1
		  },
		  target: {
		    files: {
		      'src/assets/css/style.css': [
		      	'bower_components/bootstrap/dist/css/bootstrap.css',
		      	'bower_components/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
		      	'src/assets/css/bootstrap-timepicker.css',
		      	'bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css',
		      	'src/assets/css/global.css'
		      ]
		    }
		  }
		}
	});
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.registerTask('default', ["uglify", "cssmin", 'less']);
};